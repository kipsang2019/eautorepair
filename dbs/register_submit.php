<?php 
	include 'db_conn.php';

	if (isset($_POST['signup_btn'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$national_id = mysqli_real_escape_string($db, $_POST['national_id']);
		$county = mysqli_real_escape_string($db, $_POST['county']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
		$password_2 = mysqli_real_escape_string($db, $_POST['password_2']);

		if (empty($first_name) || empty(last_name) || empty($email) || empty($national_id) || empty($county) || empty($username) || empty($password_1) || empty($password_2)) {
			header("Location: ../register.php?some fields=empty!!");
			exit();
		}else{
			//check if nationa id is taken
			$sql = "SELECT national_id FROM users WHERE national_id='$national_id'";
			$result = mysqli_query($db, $sql);
			if (mysqli_num_rows($result) > 0) {
				header("Location: ../register.php?That ID is taken!!");
				exit();
			}else{
				//check if username is taken
				$query = "SELECT username FROM users WHERE username='$username'";
				$result1 = mysqli_query($db, $sql);
				if (mysqli_num_rows($result1) > 0) {
					header("Location: ../register.php?That username is taken!!");
					exit();
				}else{
					//check if two passwords match
					if ($password_2 !== $password_1) {
						header("Location: ../register.php?The two passwords dont match!!");
						exit();
					}else{
						//insert user
						//hash password

						$pwd = sha1($password_1);

						$insert = "INSERT INTO users(first_name,last_name,email,national_id,county,username,password)
						VALUES('$first_name','$last_name','$email','$national_id','$county','$username','$pwd')";
						mysqli_query($db, $insert);
						header("Location: ../login.php?");
						exit();
					}
				}
			}
		}
	}

	//login

	if (isset($_POST['login_btn'])) {
		
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username ) || empty($password)) {
			header("Location: ../login.php?Empty fields");
			exit();
		}else{

			//hashed password
			$pwd = sha1($password);

			$sql = "SELECT * FROM users WHERE username='$username' AND password='$pwd'";
			$results = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($results);
			$row = mysqli_fetch_assoc($results);
			if ($checkResult == 1) {
				//login the user

				$_SESSION['u_id'] = $row['user_id'];
				$_SESSION['first'] = $row['first_name'];
				$_SESSION['last'] = $row['last_name'];
				$_SESSION['n_id'] = $row['national_id'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['county'] = $row['county'];

				$_SESSION['text'] = "Welcome ". $row['first_name'];

				header("Location: ../home.php");
				exit();
			}else{
				header("Location: ../login.php?login=error!!");
				exit();
			}
		}
	}

	if (isset($_POST['logout_btn'])) {
		session_start();
		session_unset();
		session_destroy();
		header("Location: ../login.php");
		exit();
	}

 ?>