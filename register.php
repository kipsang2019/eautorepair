<?php include 'header.php'; ?>

<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <h2>Register here</h2>

        <form action="dbs/register_submit.php" method="POST" accept-charset="utf-8">
            <input class="form-control" type="text" name="first_name" placeholder="First name"><br>
            <input class="form-control" type="text" name="last_name" placeholder="Last name"><br>
            <input class="form-control" type="text" name="email" placeholder="Email"><br>
            <input class="form-control" type="text" name="national_id" placeholder="National id"><br>
            <input class="form-control" type="text" name="county" placeholder="County"><br>
            <input class="form-control" type="text" name="username" placeholder="Username"><br>
            <input class="form-control" type="text" name="password_1" placeholder="Password"><br>
            <input class="form-control" type="text" name="password_2" placeholder="Confirm password"><br>
            <button class="btn btn-outline-success" name="signup_btn">Signup</button>
            Have an account?<a href="login.php" >Login</a>
        </form>
    </div>
    <div class="col-sm-4"></div>
</div>