-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2019 at 07:53 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.1.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `garage_dbs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `username`, `password`) VALUES
(2, 'molvin', '78ca3b3455ed0a9ac23834bc48f02c311c91e37b');

-- --------------------------------------------------------

--
-- Table structure for table `mechanics`
--

CREATE TABLE `mechanics` (
  `id` int(11) NOT NULL,
  `first_name` varchar(155) NOT NULL,
  `last_name` varchar(155) NOT NULL,
  `email` varchar(155) NOT NULL,
  `location` varchar(155) NOT NULL,
  `id_no` int(10) NOT NULL,
  `contact` varchar(155) NOT NULL,
  `speciality` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mechanics`
--

INSERT INTO `mechanics` (`id`, `first_name`, `last_name`, `email`, `location`, `id_no`, `contact`, `speciality`, `password`) VALUES
(1, 'Ochieng', 'James', 'jamesochieng@gmail.com', 'kaloleni', 34114154, '0711474445', 'Body work', '597b7b3135b127ba88d70d48a0349a122ebe2a7b'),
(2, 'Juma', 'Kim', 'kimjuma@gmail.com', 'Kitale', 35447445, '0711415641', 'Engine', 'e013f182f6792b7a6988a76b3ffb0da6a4b7d88b');

-- --------------------------------------------------------

--
-- Table structure for table `service_application`
--

CREATE TABLE `service_application` (
  `id` int(11) NOT NULL,
  `car_type` varchar(250) NOT NULL,
  `description` varchar(500) NOT NULL,
  `u_id` int(5) NOT NULL,
  `m_id` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_application`
--

INSERT INTO `service_application` (`id`, `car_type`, `description`, `u_id`, `m_id`) VALUES
(1, 'Rav 4', 'Bonet was damaged', 1, 2),
(2, 'Premio', 'Engine problems', 1, 1),
(3, 'Lexus', 'side mirror', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `national_id` int(10) NOT NULL,
  `county` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `national_id`, `county`, `username`, `password`) VALUES
(1, 'Kimani', 'James', 'kimanijames@gmail.com', 35224111, 'Kirinyaga', 'kimani', '34432e2c925c34baee9fef88cf2403e1f9ebf99e'),
(2, 'Frank', 'Chirchir', 'frankline@gmail.com', 34774744, 'Bomet', 'frank', '86a8c2da8527a1c6978bdca6d7986fe14ae147fe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `mechanics`
--
ALTER TABLE `mechanics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_application`
--
ALTER TABLE `service_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `u_id` (`u_id`,`m_id`),
  ADD KEY `m_id` (`m_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mechanics`
--
ALTER TABLE `mechanics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_application`
--
ALTER TABLE `service_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `service_application`
--
ALTER TABLE `service_application`
  ADD CONSTRAINT `service_application_ibfk_1` FOREIGN KEY (`u_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_application_ibfk_2` FOREIGN KEY (`m_id`) REFERENCES `mechanics` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
