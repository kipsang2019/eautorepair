<?php 
	include 'header.php';
 ?>

 <div class="row">
 	<div class="col-sm-6">
 		<h2>Mechanics</h2>

 		<table>
 			
 			<thead>
 				<tr>
 					<th>First name</th>
 					<th>Last name</th>
 					<th>Email</th>
 					<th>Location</th>
 					<th>Contact</th>
 					<th>Speciality</th>
 					<th>Apply</th>
 				</tr>
 			</thead>
 			<tbody>
 				
 			
 				
 			

 		<?php 
 			$sql = "SELECT * FROM mechanics";
 			$result = mysqli_query($db, $sql);
 			while ($row = mysqli_fetch_assoc($result)) {
 				echo '<tr>
 						<form action="apply_mechanic.php" method="POST" accept-charset="utf-8">
		 					<td>'.$row['first_name'].'</td>
		 					<td>'.$row['last_name'].'</td>
		 					<td>'.$row['email'].'</td>
		 					<td>'.$row['location'].'</td>
		 					<td>'.$row['contact'].'</td>
		 					<td>'.$row['speciality'].'</td>
		 					<input type="hidden" name="u_id" value="'.$_SESSION['u_id'].'">
		 					<input type="hidden" name="m_id" value="'.$row['id'].'">
		 					<td><button class="btn btn-info" name="apply">Apply</button></td>
	 					</form>
	 				</tr>';
 			}
 		 ?>
 		 </tbody>
 		</table>
 	</div>	
 </div>