<?php include 'header.php'; ?>

<div class="row">
    <div class="col-sm-4"></div>
    <div class="col-sm-4">
        <h2>Login here</h2>

        <form action="dbs/register_submit.php" method="POST" accept-charset="utf-8">
            <input class="form-control" type="text" name="username" placeholder="Username"><br>
            <input class="form-control" type="text" name="password" placeholder="Password"><br>
            <button class="btn btn-outline-success" name="login_btn">Login</button>

            Don't have an account?<a href="register.php" >Register</a>
        </form>
    </div>
    <div class="col-sm-4"></div>
</div>