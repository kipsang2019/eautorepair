<?php 
	
	include 'db_conn.php';

	if (isset($_POST['register'])) {
		
		$first_name = mysqli_real_escape_string($db, $_POST['first_name']);
		$last_name = mysqli_real_escape_string($db, $_POST['last_name']);
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$location = mysqli_real_escape_string($db, $_POST['location']);
		$id_no = mysqli_real_escape_string($db, $_POST['id_no']);
		$contact = mysqli_real_escape_string($db, $_POST['contact']);
		$speciality = mysqli_real_escape_string($db, $_POST['speciality']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($first_name) || empty($last_name) || empty($location) || empty($id_no) || empty($contact) || empty($speciality) || empty($password)) {
			header("Location: ../register_mechanic.php?some fields=empty!!");
			exit();
		}else{
			//check if id is taken
			$check = "SELECT id_no FROM mechanics WHERE id_no='$id_no'";
			$result = mysqli_query($db, $check);
			if (mysqli_num_rows($result) > 0) {
				header("Location: ../register_mechanic.php?ID=taken!!");
				exit();
			}else{
				//insert mechanic
				$pass = sha1($password);
				$sql = "INSERT INTO mechanics(first_name,last_name,email,location,id_no,contact,speciality,password)
				VALUES('$first_name','$last_name','$email','$location','$id_no','$contact','$speciality','$pass')";
				mysqli_query($db, $sql);
				header("Location: register_mechanic.php?insert=success!!");
				exit();
			}
		}
	}

	if (isset($_POST['mechanic_btn'])) {
		$email = mysqli_real_escape_string($db, $_POST['email']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($email ) || empty($password)) {
			header("Location: ../mechanic_login.php?Empty fields");
			exit();
		}else{

			//hashed password
			$pwd = sha1($password);

			$sql = "SELECT * FROM mechanics WHERE email='$email' AND password='$pwd'";
			$results = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($results);
			$row = mysqli_fetch_assoc($results);
			if ($checkResult == 1) {
				//login the user

				$_SESSION['m_id'] = $row['id'];
				$_SESSION['first'] = $row['first_name'];
				$_SESSION['email'] = $row['email'];
				$_SESSION['id_no'] = $row['id_no'];
				$_SESSION['contact'] = $row['contact'];
				$_SESSION['speciality'] = $row['speciality'];

				header("Location: ../mechanic_home.php");
				exit();
			}else{
				header("Location: ../admin_index.php?login=error!!sang");
				exit();
			}
		}
	}

 ?>