<?php 
	include 'db_conn.php';

	if (isset($_POST['login_btn'])) {
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$password = mysqli_real_escape_string($db, $_POST['password']);

		if (empty($username ) || empty($password)) {
			header("Location: ../admin_index.php?Empty fields");
			exit();
		}else{

			//hashed password
			$pwd = sha1($password);

			$sql = "SELECT * FROM admins WHERE username='$username' AND password='$pwd'";
			$results = mysqli_query($db, $sql);
			$checkResult = mysqli_num_rows($results);
			$row = mysqli_fetch_assoc($results);
			if ($checkResult == 1) {
				//login the user

				$_SESSION['a_id'] = $row['admin_id'];

				header("Location: ../adminHome.php");
				exit();
			}else{
				header("Location: ../admin_index.php?login=error!!");
				exit();
			}
		}
	}

if (isset($_POST['logout_btn'])) {
	session_start();
	session_unset();
	session_destroy();
	header("Location: ../admin_index.php");
	exit();
}
 ?>