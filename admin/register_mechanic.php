<?php 
	include 'header.php';

 ?>

 <div class="row">
 	<div class="col-sm-4"></div>
 	<div class="col-sm-4">
 		<h2>Mechanic registration</h2>
 		<form action="admin_dbs/mechanic_register.php" method="POST" accept-charset="utf-8">
            <input class="form-control" type="text" name="first_name" placeholder="First name"><br>
            <input class="form-control" type="text" name="last_name" placeholder="Last name"><br>
            <input class="form-control" type="text" name="email" placeholder="Email"><br>
            <input class="form-control" type="text" name="location" placeholder="Location"><br>
            <input class="form-control" type="number" name="id_no" placeholder="ID number"><br>
            <input class="form-control" type="text" name="contact" placeholder="Contact"><br>
            <input class="form-control" type="text" name="speciality" placeholder="Speciality"><br>
            <input class="form-control" type="password" name="password" placeholder="Password"><br>
            <button class="btn btn-outline-success" name="register">Register</button>
           
        </form>
 	</div>
 	<div class="col-sm-4"></div>	
 </div>